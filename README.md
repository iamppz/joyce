# Joyce

### Usage

``` shell
yarn install
yarn run serve
```

or

``` shell
npm install
npm run serve
```

### Playground

- [**Dingtalk-like Workflow Designer**](http://122.51.84.51/#/app/process/flowDesign) /#/app/process/flowDesign
- [**AStar:**](http://122.51.84.51/#/aStar) /#/aStar
